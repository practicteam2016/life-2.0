#ifndef GRID_H
#define GRID_H
#include <SFML/Graphics.hpp>
#include "Hexagon.h"
#include "Constans.h" 

using namespace sf;
class Grid
{
public:
	int size;								//���������� ������ ����
	std::vector<std::vector<Hexagon>> grid; //���� ������ ������� �� �������� �������. ������ ������ ��� ����, ������ ������� ������ � ���� �����
	RenderWindow *window;					//��� ��������� ������
	int sizeHexagon;
	int elementCount;

	Grid(RenderWindow &_window, int _size);
	~Grid();

	void ReDraw();
	void Draw();
	void Update();
	void ScanGrid();
	void Clear();
	void LinkerHexagon();
	void RandDraw();
};
#endif GRID_H