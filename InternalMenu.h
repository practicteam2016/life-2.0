#ifndef INTERNAL_MENU_H
#define INTERNAL_MENU_H
#include"Constans.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <sstream>


using namespace sf;
class InternalMenu
{
public:
	Texture textureTypes, texturePredator, textureHerbivorous, texturePlant, textureDead,
			texturePredatorActive, textureHerbivorousActive, texturePlantActive,
			textureSize, textureSizeHexagon, textureDelay, texturePlus, textureMinus, texturePlusFocus, textureMinusFocus, texturePlusClick, textureMinusClick,
			textureRandom, textureRandomFocus, textureRandomClick,
			textureReset, textureResetFocus, textureReserClick,
			textureStartPause, textureStartPauseFocus, textureStartPauseClick,
			textureMenu, textureMenuFocus, textureMenuClick,
			textureQuit, textureQuitFocus, textureQuitClick,
			textureInternalMenuBackground;
	Sprite types, predator, herbivorous, plant, dead,
		size, sizeHexagon, delay, plusSize, minusSize, plusSizeHexagon, minusSizeHexagon, plusDelay, minusDelay,
		random,	reset,	startPause,	menu, quit, redactorType, internalMenuBackground;
	
	int menuNum = 0;

	Font font;//����� 

	int numberCircles, sizeHexagons, delays;

	InternalMenu();
	void Draw(RenderWindow &_window, bool is_update);
	int MouseControl(Vector2i mousePosition, bool is_update);
	void ControlFocus(Vector2i mousePosition);
};
#endif #define INTERNAL_MENU_H
