#include "Hexagon.h"
#include "Constans.h" 


Hexagon::Hexagon()
{
	color = dead;
}

Hexagon::~Hexagon(){}

void Hexagon::SetLife(Color _color)
{
	for (int i = 0; i < History; i++)
	{
		status[i] = 0;
	}
	color = _color;
}

void Hexagon::ItsALife() //�������� ������� ��������� ��������� ������
{
	switch (status[phase])
	{
	case 0:Germination();
		break;
	case 1:LifePlant();
		break;
	case 2:LifeHerbivorou();
		break;
	case 3:LifePredator();
		break;
	default:
		break;
	}
	status[untilPhase()] = 0;
}

void Hexagon::LifePlant()
{
	int deadsSize = 0;
	Hexagon* deads[6];
	int rival = 0, mouth = 0;
	for (int i = 0; i < boundaryHexagon.size(); i++)
	{
		switch (boundaryHexagon[i]->status[phase])
		{
		case 0: 
			deads[deadsSize] = boundaryHexagon[i];
			deadsSize++;
			break;
		case 1: 
			rival++;
			break;
		case 2: 
			mouth++;
		    break;
		case 3:
			break;
		default:
			break;
		}
	}

	if ((mouth > 2) || (rival>3)||((rival+mouth)==boundaryHexagon.size()))
	{
		color = dead;
		status[NextPhase()] = 0;
	}
	else
	{
		status[NextPhase()] = 1;
	}
}

void Hexagon::LifeHerbivorou()
{
	int deadsSize = 0;
	Hexagon* deads[6];
	int rival = 0, mouth = 0, food=0;
	for (int i = 0; i < boundaryHexagon.size(); i++)
	{
		switch (boundaryHexagon[i]->status[phase])
		{
		case 0:
			deads[deadsSize] = boundaryHexagon[i];
			deadsSize++;
			break;
		case 1: 
			food++;
		    break;
		case 2: 
			rival++;
		    break;
		case 3: 
			mouth++;
		    break;
		default:
			break;
		}
	}
	if ((mouth > 0) && (rival > 0))
	{
		int random;
		random = rand() % 5;
		if (random < 2)
		{
			color = predator;
			status[NextPhase()] = 3;
		}
		else
		{
			status[NextPhase()] = 2;
		}
	}
	if ((mouth > 1) || (food == 0)||(rival>3)&&(color!=predator))
	{
		color = dead;
		status[NextPhase()] = 0;
	}
	else
	{
		status[NextPhase()] = 2;
	}
}

void Hexagon::LifePredator()
{
	int rival = 0, food = 0;
	for (int i = 0; i < boundaryHexagon.size(); i++)
	{
		switch (boundaryHexagon[i]->status[phase])
		{
		case 0:
		   break;
		case 1: 
		   break;
		case 2: 
			food++;
		   break;
		case 3: 
			rival++;
		    break;
		default:
			break;
		}
	}

	if (food==0)
	{
		int random;
		random = rand() % 5;
		if ((random < 4)||(rival==0))
		{
			color = dead;
			status[NextPhase()] = 0;
		}
		else
		{
			status[NextPhase()] = 3;
		}
	}
	else
	{
		status[NextPhase()] = 3;
	}
}

void Hexagon::Germination()
{
	int pl = 0, herb = 0, pred = 0;
	for (int i = 0; i < boundaryHexagon.size(); i++)
	{
		switch (boundaryHexagon[i]->status[phase])
		{
		case 0:
			break;
		case 1:
			pl++;
			break;
		case 2:
			herb++;
			break;
		case 3:
			pred++;
			break;
		default:
			break;
		}
	}
	int random;
	if ((pred > 0) && (herb > 0))
	{
		random = rand() % 2;
		if (random == 0)
		{
			color = predator;
			status[NextPhase()] = 3;
		}
	}
	if ((herb > 4) && (color != plant))
	{
		color = predator;
		status[NextPhase()] = 3;
	}
	if ((pl > 0) && (herb > 0) && (herb < 4) && (color != predator))
	{
		random = rand() % 5;
		if (pred == 0)
		{
			if (random < 4)
			{
				color = herbivorou;
				status[NextPhase()] = 2;
			}
		}
		else
		{
			if (random < 2)
			{
				color = herbivorou;
				status[NextPhase()] = 2;
			}
		}
	}
	if ((pl == 6) && (color != predator))
	{
		random = rand() % 20;
		if (random ==1)
		{
			color = herbivorou;
			status[NextPhase()] = 2;
		}
	}
	if ((pl > 0) && (herb < 4) && (herb > 0) && (color != predator) && (color != herbivorou))
	{
		random = rand() % 5;
		if (random < 4)
		{
			color = plant;
			status[NextPhase()] = 1;
		}
	}
	else
	{
		if ((pl > 0) && (herb < 4) && (color != predator) && (color != herbivorou))
		{
			random = rand() % 20;
			if (random == 0)
			{
				color = plant;
				status[NextPhase()] = 1;
			}

		}
	}
};
