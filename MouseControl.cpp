#include "MouseControl.h"
#include "Constans.h" 


MouseControl::MouseControl(RenderWindow *_window, Grid *_grid)
{
	grid = _grid;
	currentColor = plant;
}

MouseControl::~MouseControl(){}

void MouseControl::ControlPosition()
{
	CircleShape hexagon(grid->sizeHexagon, 6);
	for (int i = 0; i < grid->grid.size(); i++)
	{
		for (int j = 0; j < grid->grid[i].size(); j++)
		{
			hexagon.setPosition(grid->grid[i][j].position);
			if (hexagon.getGlobalBounds().contains(Vector2f(mousePosition.x, mousePosition.y)))
			{
				if (grid->grid[i][j].color != currentColor)
				{
					grid->grid[i][j].SetLife(currentColor);
				}
				else
				{
					grid->grid[i][j].SetLife(dead);
				}
				return;
			}
		}
	}
}

void MouseControl::SetColor(Color _color)
{
	currentColor = _color;
}