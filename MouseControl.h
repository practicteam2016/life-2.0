#ifndef MOUSE_CONTROL_H
#define MOUSE_CONTROL_H
#include <SFML/Graphics.hpp>
#include "Grid.h"
#include "Constans.h"

class MouseControl
{
public:
	Color currentColor;
	Grid *grid;
	Vector2i mousePosition;
	MouseControl(RenderWindow *_window, Grid *_grid);
	~MouseControl();
	void ControlPosition();
	void SetColor(Color _color);
};
#endif MOUSE_CONTROL_H