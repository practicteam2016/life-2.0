#ifndef CONSTANS_H
#define CONSTANS_H
#include <SFML/Graphics.hpp>

using namespace sf;

const Color dead = Color::White;
const Color plant = Color::Green;
const Color herbivorou = Color::Yellow;
const Color predator = Color::Red;

const int DELAY = 10;

#endif CONSTANS_H