#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Menu.h"
#include <Windows.h>
#include <vector>
#include "Grid.h"
#include "MouseControl.h"
#include "Constans.h" 
#include "Event.h"
#include "InternalMenu.h"
#include "Menu.h"

extern bool is_menu = 1;
extern bool is_update = 0;
extern bool is_ReDraw = 0;

void main()
{
	setlocale(LC_ALL, "Russian");
	srand(time(NULL));
	Clock clock;
	RenderWindow window(VideoMode(1368, 800), "LIFE_3.0", Style::Close);


	Grid grid(window, 40);
	Menu menu;
	MouseControl mouseControl(&window, &grid);
	InternalMenu internalMenu;

	Music music;
	music.openFromFile("sounds/Kenio_Fuke_-_Soul.ogg");

	//music.play();

	////
	int STEP = 5;///
	////
	int tact = 0;

	while (window.isOpen())
	{
		window.setVerticalSyncEnabled(true);// ��� ������������ �������������
		Event event;
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}
		if (Keyboard::isKeyPressed(Keyboard::Escape))
		{
			window.close();
		}

		if (is_menu)
		{
			menu.Draw(window);
			menu.ControlFocus(Mouse::getPosition(window));
			if (Mouse::isButtonPressed(Mouse::Left))
			{
				switch (menu.MouseControl(Mouse::getPosition(window)))
				{
				case 1:
					is_menu = 0;
					is_ReDraw = 1;
					break;
				case 2:
					menu.Info(window);
					break;
				case 3:
					window.close();
					break;
				default:
					break;
				}
				sleep(milliseconds(50));
			}
		}
		else
		{
			if (Keyboard::isKeyPressed(Keyboard::Space))
			{
				if (is_update)
				{
					is_update = 0;
				}
				else
				{
					is_update = 1;
					grid.ScanGrid();
				}
			}

			internalMenu.Draw(window, is_update);
			internalMenu.ControlFocus(Mouse::getPosition(window));
			if (is_ReDraw)
			{
				grid.ReDraw();
				is_ReDraw = 0;
			}

			if (is_update)
			{
				if ((tact%STEP) == 0)
				{
					grid.Update();
					tact = 0;
				}
				grid.Draw();

				if (Mouse::isButtonPressed(Mouse::Left))
				{
					switch (internalMenu.MouseControl(Mouse::getPosition(window),is_update))
					{
					case 5:
						STEP = internalMenu.delays / DELAY;///
						break;
					case 6:
						STEP = internalMenu.delays / DELAY;///
						break;
					case 9:
						is_update = 0;
						break;
					case 10:
						window.clear();
						is_update = 0;
						is_menu = 1;
						break;
					case 11:
						window.close();
						break;
					default:
						break;
					}
					sleep(milliseconds(50));
				}
			}
			else
			{
				if (Mouse::isButtonPressed(Mouse::Left))
				{
					switch (internalMenu.MouseControl(Mouse::getPosition(window),is_update))
					{
					case 1:
						grid.size = internalMenu.numberCircles;
						grid.ReDraw();
						break;
					case 2:
						grid.size = internalMenu.numberCircles;
						grid.ReDraw();
						break;
					case 3:
						grid.sizeHexagon = internalMenu.sizeHexagons;
						grid.ReDraw();
						break;
					case 4:
						grid.sizeHexagon = internalMenu.sizeHexagons;
						grid.ReDraw();
						break;
					case 5:
						STEP = internalMenu.delays / DELAY;///
						break;
					case 6:
						STEP = internalMenu.delays / DELAY;///
						break;
					case 7:
						grid.Clear();
						break;
					case 8:
						grid.Clear();
						grid.RandDraw();
						break;
					case 9:
						is_update = 1;
						grid.ScanGrid();
						break;
					case 10:
						window.clear();
						is_update = 0;
						is_menu = 1;
						break;
					case 11:
						window.close();
						break;
					case 12:
						mouseControl.currentColor = plant;
						break;
					case 13:
						mouseControl.currentColor = predator;
						break;
					case 14:
						mouseControl.currentColor = herbivorou;
						break;
					default:
						mouseControl.mousePosition = Mouse::getPosition(window);
						mouseControl.ControlPosition();
						break;
					}
					sleep(milliseconds(50));
				}
				if (Keyboard::isKeyPressed(Keyboard::Num1))
				{
					mouseControl.currentColor = plant;
				}
				if (Keyboard::isKeyPressed(Keyboard::Num2))
				{
					mouseControl.currentColor = herbivorou;
				}
				if (Keyboard::isKeyPressed(Keyboard::Num3))
				{
					mouseControl.currentColor = predator;
				}
				if (Keyboard::isKeyPressed(Keyboard::R))
				{
					grid.Clear();
					grid.RandDraw();
				}
				grid.Draw();
			}
		}

		window.display();
		window.clear();
		sleep(milliseconds(DELAY));
		tact++;
	}
}