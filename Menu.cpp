#include "Menu.h"

using namespace sf;
Menu::Menu()
{
	textureMenuBackground.loadFromFile("images/MENU BACKGROUND.png");
	textureMenuPlay.loadFromFile("images/MENU PLAY.png"); textureMenuPlayFocus.loadFromFile("images/MENU PLAY FOCUS.png");
	textureMenuInfo.loadFromFile("images/MENU INFO.png"); textureMenuInfoFocus.loadFromFile("images/MENU INFO FOCUS.png");
	textureMenuQuit.loadFromFile("images/MENU QUIT.png"); textureMenuQuitFocus.loadFromFile("images/MENU QUIT FOCUS.png");
	
	textureMenuHistory.loadFromFile("images/History.png"); textureMenuRules.loadFromFile("images/Rules.png");


	menuBackground.setTexture(textureMenuBackground);
	menuPlay.setTexture(textureMenuPlay);
	menuInfo.setTexture(textureMenuInfo);
	menuQuit.setTexture(textureMenuQuit);
	
	menuHistory.setTexture(textureMenuHistory);
	menuRules.setTexture(textureMenuRules);

	menuBackground.setPosition(0, 0);
	menuPlay.setPosition(60, 100);
	menuInfo.setPosition(60, 200);
	menuQuit.setPosition(60, 300);

	menuHistory.setPosition(270, 160);
	menuRules.setPosition(270, 160);
}
void Menu::Draw(RenderWindow &window)
{
	window.draw(menuBackground);
	window.draw(menuPlay);
	window.draw(menuInfo);
	window.draw(menuQuit);
}

void Menu::Info(RenderWindow &window)
{
	window.draw(menuHistory);
	while (window.isOpen())
	{
		window.setVerticalSyncEnabled(true);
		if (Mouse::isButtonPressed(Mouse::Left))
		{
			window.clear();
			window.draw(menuBackground);
			window.draw(menuRules);
		}
		if (Mouse::isButtonPressed(Mouse::Right))
		{
			window.clear();
			window.draw(menuBackground);
			window.draw(menuHistory);
		}
		if (Keyboard::isKeyPressed(Keyboard::Tab))
		{
			return;
		}
		window.display();
	}
}

int Menu::MouseControl(Vector2i _mousePosition)
{
	menuNum = 0;
	if (menuPlay.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) { menuNum = 1; }
	if (menuInfo.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) { menuNum = 2; }
	if (menuQuit.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) { menuNum = 3; }
	return menuNum;
}

void Menu::ControlFocus(Vector2i _mousePosition)
{
	if (menuPlay.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) 
	{
		menuPlay.setTexture(textureMenuPlayFocus);
	}
	else
	{
		menuPlay.setTexture(textureMenuPlay);
	}
	if (menuInfo.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y)))
	{ 
		menuInfo.setTexture(textureMenuInfoFocus); 
	}
	else
	{
		menuInfo.setTexture(textureMenuInfo);
	}
	if (menuQuit.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y)))
	{
		menuQuit.setTexture(textureMenuQuitFocus);
	}
	else
	{
		menuQuit.setTexture(textureMenuQuit);
	}
}