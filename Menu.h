#ifndef MENU_H
#define MENU_H
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

using namespace sf;
class Menu
{
public:

	Texture textureMenuPlay, textureMenuPlayFocus, textureMenuInfo, textureMenuInfoFocus, textureMenuQuit, textureMenuQuitFocus, textureMenuBackground, textureMenuHistory, textureMenuRules;
	Sprite menuPlay, menuInfo, menuQuit, menuBackground, menuHistory, menuRules;
	

	int menuNum;

	Menu();
	void Draw(RenderWindow &window);
	void Info(RenderWindow &window);
	int MouseControl(Vector2i _mousePosition);
	void ControlFocus(Vector2i _mousePosition);
};
#endif MENU_H