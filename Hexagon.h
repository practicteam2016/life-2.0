#ifndef HEXAGON_H
#define HEXAGON_H
#include <SFML\Graphics.hpp>
#include <Windows.h>
#include "Constans.h" 
#include "phase.h"
#include <vector>

using namespace sf;

class Hexagon
{
public:
	Vector2f position;
	int status[History];
	Color color;
	std::vector<Hexagon*> boundaryHexagon; //����� ��������� ������

	Hexagon();
	~Hexagon();

	void ItsALife(); //�������� ������� ��������� ��������� ������
	void SetLife(Color _color);
	void LifePlant();
	void LifeHerbivorou();
	void LifePredator();
	void Germination();
};
#endif HEXAGON_H