#include "Grid.h"

using namespace sf;

Grid::Grid(RenderWindow &_window, int _size)
{
	sizeHexagon = 4;
	window = &_window;
	size = _size;

	LinkerHexagon();
}

Grid::~Grid(){}

void Grid::RandDraw()
{
	for (int Size = 0; Size < size-1; Size++)
	{
		double k = (Size / (Size + 1));
		if(Size%2 == 1)
		{
			for (int i = 1;i < Size * 6; i++)
			{
				int Coef1 = rand() %50 + 30;
				int Random1 = rand() % 100;
				if (Random1 > Coef1) // ����������, ����� �� ������ ����
				{
					int Random2 = rand() % 4;
					switch (Random2)// ���������� �������������� ������ � ��������
					{
					case 0:
						grid[Size][i].color = predator;
						break;
					case 1:
						grid[Size][i].color = herbivorou;
						break;
					case 2:
						grid[Size][i].color = plant;
						break;
					case 3:
						grid[Size][i].color = predator;
						break;
					default:
						grid[Size][i].color = dead;
						break;
					}

					int Random3 = rand() % 2;
					switch (Random3)// ���������� ������� ���� ������ �� ������ ����� �����
					{
					case 0:
						grid[Size+1][int(i*k)].color = grid[Size][i].color;
						grid[Size + 1][int(i*k) + 2].color = grid[Size][i].color;
						break;
					case 1:
						grid[Size + 1][int(i*k)+1].color = grid[Size][i].color;
						grid[Size + 1][int(i*k) + 2].color = grid[Size][i].color;
						break;
					case 2:
						grid[Size + 1][(int)i*k].color = grid[Size][i].color;
						grid[Size + 1][int(i*k) + 1].color = grid[Size][i].color;
						break;
					default:
						break;
					}
				}
			}
		}	else continue;
	}

}

void Grid::ReDraw() {
	for (int i = 0; i < grid.size(); i++)
	{
		grid[i].clear();
	}
	grid.clear();
	LinkerHexagon();
	CircleShape hexagon(sizeHexagon, 6);
	hexagon.rotate(30);
	int direction = 1;
	int offset = sizeHexagon * 2;
	elementCount = 0;

	std::vector<Hexagon*> livings;

	for (register int i = 0; i < grid.size(); i++)
	{
		hexagon.setPosition(450, 420 - offset*i);
		for (register int j = 0; j < grid[i].size(); j++)
		{
			elementCount++;
			switch (direction)
			{
			case 1:
				hexagon.move(0, -offset);		   //�����
				break;
			case 2:
				hexagon.move(-offset, offset*0.5); //����� ����
				break;
			case 3:
				hexagon.move(0, offset);		   //����
				break;
			case 4:
				hexagon.move(offset, offset*0.5);  //������ ����
				break;
			case 5:
				hexagon.move(offset, -offset*0.5); //������ �����
				break;
			case 6:
				hexagon.move(0, -offset);		   //�����
				break;
			case 7:
				hexagon.move(-offset, -offset*0.5);//����� �����
				break;
			default:
				break;
			}
			grid[i][j].position = hexagon.getPosition();

			hexagon.setFillColor(grid[i][j].color);
			window->draw(hexagon);

			if ((i == 0) || (j == 0))
			{
				direction++;
			}
			else
			{
				if (j%i == 0)
				{
					direction++;
				}
			}
		}
		direction = 1;
	}
}

void Grid::Draw()
{
	CircleShape hexagon(sizeHexagon, 6);
	hexagon.rotate(30);
	for (register int i = 0; i < grid.size(); i++)
	{
		for (int j = 0; j < grid[i].size(); j++)
		{
			hexagon.setPosition(grid[i][j].position);
			hexagon.setFillColor(grid[i][j].color);
			window->draw(hexagon);
		}
	}
}

void Grid::Update()
{
	for (register int i = 0; i < grid.size(); i++)
	{
		for (register int j = 0; j < grid[i].size(); j++)
		{
			grid[i][j].ItsALife();
		}
	}
	SwitchPhase();
}

void Grid::ScanGrid()
{
	for (register int i = 0; i < grid.size(); i++)
	{
		for (register int j = 0; j < grid[i].size(); j++)
		{
			if (grid[i][j].color == dead)
			{
				grid[i][j].status[phase] = 0;
			}
			if (grid[i][j].color == plant)
			{
				grid[i][j].status[phase] = 1;
			}
			if (grid[i][j].color == herbivorou)
			{
				grid[i][j].status[phase] = 2;
			}
			if (grid[i][j].color == predator)
			{
				grid[i][j].status[phase] = 3;
			}
		}
	}
}

void Grid::Clear()
{
	for (int i = 0; i < grid.size(); i++)
	{
		for (int j = 0; j < grid[i].size(); j++)
		{
			grid[i][j].SetLife(dead);
		}
	}
}

void Grid::LinkerHexagon()
{
	grid.resize(size); //����������� ������ ��� ������ �������(�����)

	grid[0].resize(1); //����������� �������

	for (int i = 1; i < size; i++)//�������������� ������ ��� ��������� �����
	{
		grid[i].resize(i * 6);
	}

	for (int i = 0; i < grid[1].size(); i++)
	{
		grid[0][0].boundaryHexagon.push_back(&grid[1][i]);
		grid[1][i].boundaryHexagon.push_back(&grid[0][0]);
		if (i != (grid[1].size() - 1))
		{
			grid[1][i].boundaryHexagon.push_back(&grid[1][i + 1]);
			grid[1][i + 1].boundaryHexagon.push_back(&grid[1][i]);
		}
		else
		{
			grid[1][i].boundaryHexagon.push_back(&grid[1][0]);
			grid[1][0].boundaryHexagon.push_back(&grid[1][i]);
		}
	}

	for (int i = 2; i < grid.size(); i++)
	{
		for (int j = 0, k = 0; j < grid[i].size(); j++, k++)
		{
			if ((j%i == 0) && (k != 0))
			{
				k--;
				grid[i][j].boundaryHexagon.push_back(&grid[i - 1][k]);
				grid[i - 1][k].boundaryHexagon.push_back(&grid[i][j]);
				grid[i][j].boundaryHexagon.push_back(&grid[i][j + 1]);
				grid[i][j + 1].boundaryHexagon.push_back(&grid[i][j]);
			}
			else
			{
				if (k == 0)
				{
					grid[i][j].boundaryHexagon.push_back(&grid[i - 1][k]);
					grid[i - 1][k].boundaryHexagon.push_back(&grid[i][j]);
					grid[i][j].boundaryHexagon.push_back(&grid[i][j + 1]);
					grid[i][j + 1].boundaryHexagon.push_back(&grid[i][j]);
				}
				else
				{
					if (k != grid[i - 1].size())
					{
						grid[i][j].boundaryHexagon.push_back(&grid[i - 1][k]);
						grid[i - 1][k].boundaryHexagon.push_back(&grid[i][j]);
					}
					else
					{
						grid[i][j].boundaryHexagon.push_back(&grid[i - 1][0]);
						grid[i - 1][0].boundaryHexagon.push_back(&grid[i][j]);
					}
					grid[i][j].boundaryHexagon.push_back(&grid[i - 1][k - 1]);
					grid[i - 1][k - 1].boundaryHexagon.push_back(&grid[i][j]);
					if (j != grid[i].size() - 1)
					{
						grid[i][j].boundaryHexagon.push_back(&grid[i][j + 1]);
						grid[i][j + 1].boundaryHexagon.push_back(&grid[i][j]);
					}
					else
					{
						grid[i][j].boundaryHexagon.push_back(&grid[i][0]);
						grid[i][0].boundaryHexagon.push_back(&grid[i][j]);
					}
				}
			}
		}
	}
}