#include "InternalMenu.h"
using namespace sf;

InternalMenu::InternalMenu()
{
	font.loadFromFile("Rubius.ttf"); //�������� ������ ������ ���� ������

	textureInternalMenuBackground.loadFromFile("images/GAME BACKGROUND.png");

	textureTypes.loadFromFile("images/TYPES.png");

	texturePredator.loadFromFile("images/PREDATOR.png");
	textureHerbivorous.loadFromFile("images/HERBIVOROUS.png");
	texturePlant.loadFromFile("images/PLANT.png");
	textureDead.loadFromFile("images/DEAD.png");

	texturePredatorActive.loadFromFile("images/PREDATOR ACTIVE.png");
	textureHerbivorousActive.loadFromFile("images/HERBIVOROUS ACTIVE.png");
	texturePlantActive.loadFromFile("images/PLANT ACTIVE.png");

	textureSize.loadFromFile("images/SIZE.png");
	textureSizeHexagon.loadFromFile("images/SIZE HEXAGONS.png");
	textureDelay.loadFromFile("images/DELAY.png");

	texturePlus.loadFromFile("images/PLUS.png");
	texturePlusFocus.loadFromFile("images/PLUS FOCUS.png");
	texturePlusClick.loadFromFile("images/PLUS CLICK.png");
	
	textureMinus.loadFromFile("images/MINUS.png");
	textureMinusFocus.loadFromFile("images/MINUS FOCUS.png");
	textureMinusClick.loadFromFile("images/MINUS CLICK.png");

	textureRandom.loadFromFile("images/RANDOM.png");
	textureRandomFocus.loadFromFile("images/RANDOM FOCUS.png");
	textureRandomClick.loadFromFile("images/RANDOM CLICK.png");

	textureReset.loadFromFile("images/RESET.png");
	textureResetFocus.loadFromFile("images/RESET FOCUS.png");
	textureReserClick.loadFromFile("images/RESET CLICK.png");

	textureStartPause.loadFromFile("images/START PAUSE.png");
	textureStartPauseFocus.loadFromFile("images/START PAUSE FOCUS.png");
	textureStartPauseClick.loadFromFile("images/START PAUSE CLICK.png");

	textureMenu.loadFromFile("images/MENU.png");
	textureMenuFocus.loadFromFile("images/MENU FOCUS.png");
	textureMenuClick.loadFromFile("images/MENU CLICK.png");

	textureQuit.loadFromFile("images/QUIT.png");
	textureQuitFocus.loadFromFile("images/QUIT FOCUS.png");
	textureQuitClick.loadFromFile("images/QUIT CLICK.png");

	internalMenuBackground.setTexture(textureInternalMenuBackground);
	types.setTexture(textureTypes);
	predator.setTexture(texturePredator);
	herbivorous.setTexture(textureHerbivorous);
	plant.setTexture(texturePlant);
	dead.setTexture(textureDead);
	size.setTexture(textureSize);
	sizeHexagon.setTexture(textureSizeHexagon);
	delay.setTexture(textureDelay);
	plusSize.setTexture(texturePlus);
	minusSize.setTexture(textureMinus);
	plusSizeHexagon.setTexture(texturePlus);
	minusSizeHexagon.setTexture(textureMinus);
	plusDelay.setTexture(texturePlus);
	minusDelay.setTexture(textureMinus);
	random.setTexture(textureRandom);
	reset.setTexture(textureReset);
	startPause.setTexture(textureStartPause);
	menu.setTexture(textureMenu);
	quit.setTexture(textureQuit);
	redactorType.setTexture(texturePlantActive);

	internalMenuBackground.setPosition(0,0);
	types.setPosition(950,10);
	redactorType.setPosition(815,70);
	predator.setPosition(950,70);
	herbivorous.setPosition(1160,70);
	plant.setPosition(950,130);
	dead.setPosition(1160,130);
	size.setPosition(1125, 200);
	sizeHexagon.setPosition(1125,320);
	delay.setPosition(1130,440);
	minusSize.setPosition(1040,260);
	minusSizeHexagon.setPosition(1040,380);
	minusDelay.setPosition(1040,500);
	plusSize.setPosition(1300,260);
	plusSizeHexagon.setPosition(1300,380);
	plusDelay.setPosition(1300,500);
	random.setPosition(1040,580);
	reset.setPosition(1200,580);
	startPause.setPosition(1040,655);
	menu.setPosition(1040,720);
	quit.setPosition(1200,720);

	numberCircles = 40;  sizeHexagons = 4;  delays = DELAY * 5;///
}

void InternalMenu::Draw(RenderWindow &window, bool is_update)
{
	Text text1("", font, 50);
	Text text2("", font, 50);
	Text text3("", font, 50);

	text1.setStyle(Text::Bold);
	text2.setStyle(Text::Bold);
	text3.setStyle(Text::Bold);

	text1.setPosition(1170, 250);
	text2.setPosition(1170, 370);
	text3.setPosition(1170, 490);

	std::ostringstream _numberCircles;
	_numberCircles << numberCircles;
	text1.setString(_numberCircles.str());

	std::ostringstream _sizeHexagons;
	_sizeHexagons << sizeHexagons;
	text2.setString(_sizeHexagons.str());

	std::ostringstream _delays;
	_delays << delays;
	text3.setString(_delays.str());

	window.draw(internalMenuBackground);
	window.draw(types);
	if (!is_update)
	{
		window.draw(redactorType);
	}
	window.draw(predator);
	window.draw(herbivorous);
	window.draw(plant);
	window.draw(dead);
	window.draw(size);
	window.draw(sizeHexagon);
	window.draw(delay);
	window.draw(plusSize);
	window.draw(minusSize);
	window.draw(plusSizeHexagon);
	window.draw(minusSizeHexagon);
	window.draw(plusDelay);
	window.draw(minusDelay);
	window.draw(random);
	window.draw(reset);
	window.draw(startPause);
	window.draw(menu);
	window.draw(quit);

	window.draw(text1);
	window.draw(text2);
	window.draw(text3);
}

int InternalMenu::MouseControl(Vector2i _mousePosition, bool is_update)
{
	menuNum = 0;

	if (minusSize.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) { minusSize.setTexture(textureMinusClick); menuNum = 1; }
	if (plusSize.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) { plusSize.setTexture(texturePlusClick); menuNum = 2; }
	if (minusSizeHexagon.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) { minusSizeHexagon.setTexture(textureMinusClick); menuNum = 3; }
	if (plusSizeHexagon.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) { plusSizeHexagon.setTexture(texturePlusClick); menuNum = 4; }
	if (minusDelay.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) {  minusDelay.setTexture(textureMinusClick); menuNum = 5; }
	if (plusDelay.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) {  plusDelay.setTexture(texturePlusClick); menuNum = 6; }
	if (reset.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) {  reset.setTexture(textureReserClick); menuNum = 7; }
	if (random.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) {  random.setTexture(textureRandomClick); menuNum = 8; }
	if (startPause.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) {  startPause.setTexture(textureStartPauseClick); menuNum = 9; }
	if (menu.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) {  menu.setTexture(textureMenuClick); menuNum = 10; }
	if (quit.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) {  quit.setTexture(textureQuitClick); menuNum = 11; }
	if (plant.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) {  menuNum = 12; }
	if (predator.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) {   menuNum = 13; }
	if (herbivorous.getGlobalBounds().contains(Vector2f(_mousePosition.x, _mousePosition.y))) {  menuNum = 14; }

	if (!is_update)
	{
		if (menuNum == 1)
		{
			if (numberCircles - 1 > 2)
			{
				numberCircles -= 1;
			}
		}

		if (menuNum == 2)
		{
			numberCircles += 1;
		}

		if (menuNum == 3)
		{
			if (sizeHexagons - 1 > 0)
			{
				sizeHexagons -= 1;
			}
		}

		if (menuNum == 4)
		{
			sizeHexagons += 1;
		}

		if (menuNum == 12)
		{
			redactorType.setTexture(texturePlantActive);
		}

		if (menuNum == 13)
		{
			redactorType.setTexture(texturePredatorActive);
		}

		if (menuNum == 14)
		{
			redactorType.setTexture(textureHerbivorousActive);
		}
	}

	if (menuNum == 5)
	{
		if (delays - 10 > 0)
		{
			delays -= 10;
		}
	}

	if (menuNum == 6)
	{
		delays += 10;
	}

	sleep(milliseconds(50));
	return menuNum;
}

void InternalMenu::ControlFocus(Vector2i mousePosition)
{
	if (minusSize.getGlobalBounds().contains(Vector2f(mousePosition.x, mousePosition.y)))
	{ 
		minusSize.setTexture(textureMinusFocus);
	}
	else
	{
		minusSize.setTexture(textureMinus);
	}
	
	if (plusSize.getGlobalBounds().contains(Vector2f( mousePosition.x,  mousePosition.y))) 
	{

		plusSize.setTexture(texturePlusFocus);
	}
	else
	{
		plusSize.setTexture(texturePlus);
	}

	if (minusSizeHexagon.getGlobalBounds().contains(Vector2f( mousePosition.x,  mousePosition.y)))
	{ 
		minusSizeHexagon.setTexture(textureMinusFocus);
	}
	else
	{
		minusSizeHexagon.setTexture(textureMinus);
	}
	
	if (plusSizeHexagon.getGlobalBounds().contains(Vector2f( mousePosition.x,  mousePosition.y))) 
	{
		plusSizeHexagon.setTexture(texturePlusFocus);
	}
	else
	{
		plusSizeHexagon.setTexture(texturePlus);
	}
	
	if (minusDelay.getGlobalBounds().contains(Vector2f( mousePosition.x,  mousePosition.y))) 
	{
		minusDelay.setTexture(textureMinusFocus);
	}
	else
	{
		minusDelay.setTexture(textureMinus);
	}
	
	if (plusDelay.getGlobalBounds().contains(Vector2f( mousePosition.x,  mousePosition.y)))
	{
		plusDelay.setTexture(texturePlusFocus);
	}
	else
	{
		plusDelay.setTexture(texturePlus);
	}
	
	if (reset.getGlobalBounds().contains(Vector2f( mousePosition.x,  mousePosition.y))) 
	{
		reset.setTexture(textureResetFocus);
	}
	else
	{
		reset.setTexture(textureReset);
	}
	
	if (random.getGlobalBounds().contains(Vector2f( mousePosition.x,  mousePosition.y))) 
	{
		random.setTexture(textureRandomFocus);
	}
	else
	{
		random.setTexture(textureRandom);
	}
	
	if (startPause.getGlobalBounds().contains(Vector2f( mousePosition.x,  mousePosition.y))) 
	{
		startPause.setTexture(textureStartPauseFocus);
	}
	else
	{
		startPause.setTexture(textureStartPause);
	}
	
	if (menu.getGlobalBounds().contains(Vector2f( mousePosition.x,  mousePosition.y))) 
	{
		menu.setTexture(textureMenuFocus);
	}
	else
	{
		menu.setTexture(textureMenu);
	}
	
	if (quit.getGlobalBounds().contains(Vector2f( mousePosition.x,  mousePosition.y))) 
	{
		quit.setTexture(textureQuitFocus);
	}
	else
	{
		quit.setTexture(textureQuit);
	}
}